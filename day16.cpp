#define FIELD_COUNT 20
// #define TEST
#include "day16.h"


int main() {

#ifdef TEST

#undef FIELD_COUNT
#define FIELD_COUNT 3
#define LAST_IMPORTAN_FIELD 2
     u32 rules[3][4] {
         {0,1,4,19}, // class
         {0,5,8,19}, // row 
         {0,13,16,19}, // seat
     };

     ticket tickets[4] = {
         {11,12,13},
         {3,9,18},
         {15,1,5},
         {5,14,9},
     };
#else

#define LAST_IMPORTAN_FIELD 6

#endif



    u32 *invalidTickets = (u32*)(malloc(sizeof(u32) * ARRAY_COUNT(tickets)));
    memset(invalidTickets, 1, sizeof(u32) * ARRAY_COUNT(tickets));

    u32 scanningErrorRate = 0;
    for (u32 i = 0; i < ARRAY_COUNT(tickets); ++i) {
        ticket t = tickets[i];

        // loop through fields
        for (u32 j = 0; j < FIELD_COUNT; ++j) {
            u32 v = t.fields[j];
            // loop through rules
            u32 valid = 0;
            for (u32 k = 0; k < FIELD_COUNT; ++k) {
                valid = valid || (
                        (v >= rules[k][0] && v <= rules[k][1])
                     || (v >= rules[k][2] && v <= rules[k][3]));
            }
            if (!valid) {
                // printf("invalid field: %u\n", v);
                scanningErrorRate += v;
                invalidTickets[i] = 0;
                break;
            }
        }
    }

    printf("scanning error rate is %u\n", scanningErrorRate);

    u32 validation_matrix[FIELD_COUNT][FIELD_COUNT] = {0};


    // loop through rules
    for (u32 j = 0; j < FIELD_COUNT; ++j) {
        u32 *rule = rules[j];
        // loop through fields
        for (u32 k = 0; k < FIELD_COUNT; ++k) {
            u32 valid = 1;
            // loop through tickets
            for (u32 i = 0; i < ARRAY_COUNT(tickets); ++i) {
                if (!invalidTickets[i]) continue;

                ticket t = tickets[i];
                u32 v = t.fields[k];

                valid = valid && (
                        (v >= rule[0] && v <= rule[1])
                     || (v >= rule[2] && v <= rule[3]));
            }
            if(valid) {
                validation_matrix[k][j] = 1;
                // printf("field %u is valid for all ticket using rule %u\n", k, j);
            }
        }
    }

    printf("done scanning tickets, validation_matrix is:\n");
    for(u32 i = 0; i < FIELD_COUNT; ++i) {
        for(u32 j = 0; j < FIELD_COUNT; ++j) {
            printf("%u,", validation_matrix[i][j]);
        }
        printf("\n");
    }


    // this maps from fule to field
    u32 field_map[FIELD_COUNT] = {0};
    memset(field_map, -1, sizeof(u32)*FIELD_COUNT);

    // this tracks fields that have been identified
    u32 done_map[FIELD_COUNT] = {0};
    memset(done_map, 0, sizeof(u32)*FIELD_COUNT);

    u32 foundCount = 0;
    while (foundCount < FIELD_COUNT) {

        u32 found1 = 0;

        for(u32 field = 0; field < FIELD_COUNT; ++field)
        {
            // skip if field has already benn identified
            if(done_map[field]) continue;

            // count matching rules
            u32 count = 0;
            u32 rule = -1;
            for(u32 j = 0; j < FIELD_COUNT; ++j)
            {
                count += validation_matrix[field][j];
                if (validation_matrix[field][j]) rule = j;
            }

            if(count == 1) {
                found1 = 1;
                foundCount++;
                // this rule matches only one field so we marke it as invalid
                // for all other fields
                for(u32 k = 0; k < FIELD_COUNT; ++k)
                {
                    if (k == field) continue;
                    validation_matrix[k][rule] = 0;
                }
                field_map[rule] = field;
                done_map[field] = rule;
                // printf("field %u is rule %u\n", field, rule);
            }
        }
        if(!found1) {
            printf("we're stuck\n");
            exit(1);
            break;
        }
    }

    u64 total = 1;
    for(u32 i = 0; i < LAST_IMPORTAN_FIELD; ++i) {
        u32 v = tickets[0].fields[field_map[i]];
        total *= v;
        printf("ticket[%u] = %u\n ", i, v);
    }
    printf("total prod: %lu\n", total);


    return 0;
}
