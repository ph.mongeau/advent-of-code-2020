#include <stdlib.h>
#include <stdio.h>
#include "utils.h"

#define ARRAY_COUNT(a) (sizeof(a)/sizeof(a[0]))

#define PART2

static char test_data[] = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc\n";


int main() {
    int min = 0, max = 0;
    char c = {};
    char passwd[128] = {};
    int numChar = 0;
    char *data = 0;
    read_file("day02_input.txt", &data);

    int validCount = 0;
    char *ptr = data;

    while(*ptr) {
        sscanf(ptr, "%i-%i %c: %s\n%n", &min, &max, &c, (char*)(&passwd), &numChar);
        ptr += numChar;
        int count = 0;

#ifdef PART1
        // validate part 1
        for (int i = 0; passwd[i]; ++i)
        {
            if (passwd[i] == c) count++;
        }
        if (count <= max && count >= min) validCount++;
#else
        // validate part 2
        if ((passwd[min-1] == c) ^ (passwd[max-1] == c)) validCount++;
#endif
    }
    free(data);

    printf("\nNumber of valid passwords: %i\n", validCount);
    return 0;
}
