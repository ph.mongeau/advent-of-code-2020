#include "utils.h"
#include <string.h>
#include <stdlib.h>

#define PREAMBLE 25

int cmp_int(const void *a, const void *b) {
    return *((int*)a) > *((int*)b) ? 1 : -1;
}

void printGrid(char *grid, u32 lineCount, u32 columnCount) {
    for(u32 y = 1; y <= lineCount; ++y)
    {
        for(u32 x = 1; x <= columnCount; ++x)
        {
            char c = grid[y*(columnCount+2)+x];
            printf("%c", c);
        }
        printf("\n");
    }
}

bool findInDir(char *grid, u32 lineCount, u32 columnCount, u32 x, u32 y, u32 dx, u32 dy) {
    for(;;)  
    {
        x += dx;
        y += dy;
        char c = grid[y*(columnCount+2)+x];
        if (c == '#') return true;
        if (c == 0 || c == 'L') return false;

    }
    return false;
}
u32 getCount(char *grid, u32 lineCount, u32 columnCount, u32 x, u32 y) {
    u32 count = 
        + findInDir(grid, lineCount, columnCount, x, y, +0, +1)
        + findInDir(grid, lineCount, columnCount, x, y, +0, -1)
        + findInDir(grid, lineCount, columnCount, x, y, +1, +0)
        + findInDir(grid, lineCount, columnCount, x, y, -1, +0)
        + findInDir(grid, lineCount, columnCount, x, y, +1, +1)
        + findInDir(grid, lineCount, columnCount, x, y, -1, -1)
        + findInDir(grid, lineCount, columnCount, x, y, +1, -1)
        + findInDir(grid, lineCount, columnCount, x, y, -1, +1);
    return count;
}

int main() {

    char *data;
    u32 fsize = read_file("day11_input.txt", &data);
    u32 lineCount = line_count(data, fsize);
    u32 columnCount = find_char(data, '\n');

    char grid[lineCount+2][columnCount+2];
    char nextGrid[lineCount+2][columnCount+2];
    u32 gridSize = sizeof(char)*(lineCount+2)*(columnCount+2);

    memset(grid, 0, gridSize);

    for(u32 y = 0; y < lineCount; ++y)
    {
        for(u32 x = 0; x < columnCount; ++x)
        {
            char c = data[y*(columnCount+1) + x];
            grid[y+1][x+1] = c;
        }
    }



    memcpy(nextGrid, grid, gridSize);
    printGrid((char*)grid, lineCount, columnCount);
    printf("^^^^^^^^^^^^^^\n");
    u32 occupiedCount = 0;

    /*
     * If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
     * If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
     */
#ifdef PART1
    for (;;)
    {
        occupiedCount = 0;
        for(u32 y = 1; y <= lineCount; ++y)
        {
            for(u32 x = 1; x <= columnCount; ++x)
            {
                char c = grid[y][x];
                u32 count = (u32)(grid[y][x+1] == '#')
                          + (u32)(grid[y][x-1] == '#')
                          + (u32)(grid[y+1][x] == '#')
                          + (u32)(grid[y-1][x] == '#')
                          + (u32)(grid[y+1][x+1] == '#')
                          + (u32)(grid[y-1][x-1] == '#')
                          + (u32)(grid[y+1][x-1] == '#')
                          + (u32)(grid[y-1][x+1] == '#');

                if (grid[y][x] == 'L' && count == 0)
                {
                    nextGrid[y][x] = '#';
                }
                else if (grid[y][x] == '#'
                         && count >= 4)
                {
                    nextGrid[y][x] = 'L';
                }
                occupiedCount += nextGrid[y][x] == '#';
            }
        }
        if (memcmp(grid, nextGrid, gridSize) == 0)
            break;

        memcpy(grid, nextGrid, gridSize);
        printGrid((char*)grid, lineCount, columnCount);
        printf("------\n");

    }

#else
    for (;;)
    {
        occupiedCount = 0;
        for(u32 y = 1; y <= lineCount; ++y)
        {
            for(u32 x = 1; x <= columnCount; ++x)
            {
                char c = grid[y][x];
                u32 count = findInDir((char*)grid, lineCount, columnCount, x, y, +0, +1)
                          + findInDir((char*)grid, lineCount, columnCount, x, y, +0, -1)
                          + findInDir((char*)grid, lineCount, columnCount, x, y, +1, +0)
                          + findInDir((char*)grid, lineCount, columnCount, x, y, -1, +0)
                          + findInDir((char*)grid, lineCount, columnCount, x, y, +1, +1)
                          + findInDir((char*)grid, lineCount, columnCount, x, y, -1, -1)
                          + findInDir((char*)grid, lineCount, columnCount, x, y, +1, -1)
                          + findInDir((char*)grid, lineCount, columnCount, x, y, -1, +1);

                if (grid[y][x] == 'L' && count == 0)
                {
                    nextGrid[y][x] = '#';
                }
                else if (grid[y][x] == '#'
                         && count >= 5)
                {
                    nextGrid[y][x] = 'L';
                }
                occupiedCount += nextGrid[y][x] == '#';
            }
        }
        if (memcmp(grid, nextGrid, gridSize) == 0)
            break;

        memcpy(grid, nextGrid, gridSize);
        // printGrid((char*)grid, lineCount, columnCount);
        // printf("------\n");

    }
#endif

    printf("number of occupied seats: %u\n", occupiedCount);
}
