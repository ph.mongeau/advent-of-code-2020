#include "utils.h"
#include <string.h>
#include <stdlib.h>

#define PREAMBLE 25

int cmp_int(const void *a, const void *b) {
    return *((int*)a) > *((int*)b) ? 1 : -1;
}

int main() {

    char *data;
    u32 fsize = read_file("day10_input.txt", &data);
    u32 lineCount = line_count(data, fsize);
    u32 numbers[lineCount];

    { // load the numbers
        u32 i = 0;
        u32 offset = 0;
        for(char *ptr = data; *ptr;) {
            sscanf(ptr, "%u\n%n", &numbers[i++], &offset);
            ptr += offset;
        }
    }
    qsort(numbers, lineCount, sizeof(u32), cmp_int);
    u32 ones = 0;
    u32 threes = 1; // device is always 3 more than largest adapter

    // ones += 3 - numbers[0] == 1;
    // threes += 3 - numbers[0] == 3;

    // printf("result %u * %u = %u\n", ones, threes, ones*threes);
    u32 prev = 0;

    for(u32 i = 0; i < lineCount; ++i) {
        u32 delta = numbers[i] - prev;
        prev = numbers[i];
        // printf("%u: +%u\n", numbers[i], delta);
        ones += delta == 1;
        threes += delta == 3;
    }

    // count number of valid permutations
    // move down starting from the device:
    // if the delta is < 3 skip 1s until delta <= 3.
    // each skip is a new permutation?
    //
    //  1   2  1  1  1  3   1   1   3  1   3   3   3
    // (0), 1, 4, 5, 6, 7, 10, 11, 12, 15, 16, 19, (22)
    //
    // We now there's only threes and ones, does that help?
    //
    printf("part1:  %u * %u = %u\n", ones, threes, ones*threes);

    prev = numbers[lineCount-1] + 3;
    u32  acc = 0;
    u32 skips = 0;
    for(u32 i = lineCount - 1; i > 0; --i) {
        u32 delta = prev - numbers[i];
        printf("%u: +%u", numbers[i], delta);
        // u32 j = i - 1;
        while(delta < 3 && i > 0) {
            printf("   skipped %u, %u ", numbers[i], delta);
            delta += prev - numbers[i];
            skips++;
            --i;
        }
        printf("\n");
        if (i > 0) prev = numbers[i];
        else break;
    }
    printf("part2: skipCount %u", skips);
    return 0;
}
