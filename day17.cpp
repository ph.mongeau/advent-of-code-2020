#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define GRID_DIM 128
#define GRID_SIZE GRID_DIM*GRID_DIM*GRID_DIM*GRID_DIM


struct bound {
    u32 minZ = GRID_DIM/2;
    u32 sizeZ = 1;

    u32 minY = GRID_DIM/2;
    u32 sizeY = 1;

    u32 minX = GRID_DIM/2;
    u32 sizeX = 1;

    u32 minW = GRID_DIM/2;
    u32 sizeW = 1;
};

u32 get_coord(u32 x, u32 y, u32 z, u32 w) {
           // Z                                Y                           X
    // return ((GRID_DIM/2)*GRID_DIM*GRID_DIM) + ((GRID_DIM/2) * GRID_DIM) + (GRID_DIM/2);
    return (w*GRID_DIM*GRID_DIM*GRID_DIM) + (z*GRID_DIM*GRID_DIM) + (y * GRID_DIM) + (x);
}

char count_neighbours(char *grid, u32 x, u32 y, u32 z, u32 w) {
    char count = 0;

    for(u32 W = w-1; W <= w+1; W+=1)
    {
        for(u32 Z = z-1; Z <= z+1; Z+=1)
        {
            for(u32 Y = y-1; Y <= y+1; Y+=1)
            {
                for(u32 X = x-1; X <= x+1; X+=1)
                {
                    if(x == X && y == Y && z == Z && w == W) continue;
                    else count += grid[get_coord(X,Y,Z,W)];
                }
            }
        }
    }

    return count;
}

int main() {

    char *grid = (char*)malloc(sizeof(char) * GRID_SIZE);
    memset(grid, 0, GRID_SIZE);

    // current bounds
    bound Bounds{};

#if 0
     char start[3][3] = {
         {0,1,0},
         {0,0,1},
         {1,1,1},
     };
    
    for(u32 i = 0; i < 3; ++i) {
        for(u32 j = 0; j < 3; ++j) {
            grid[get_coord((GRID_DIM/2)+i, (GRID_DIM/2)+j, GRID_DIM/2, GRID_DIM/2)] = start[j][i];
        }
    }
    // update bounds:
    // bounds need to inclue 1 more in each direction so we evaluate all square that can possibly change
    Bounds.minX = GRID_DIM/2 - 1;
    Bounds.minY = GRID_DIM/2 - 1;
    Bounds.minZ = GRID_DIM/2 - 1;
    Bounds.minW = GRID_DIM/2 - 1;
    Bounds.sizeY += 5;
    Bounds.sizeX += 6;
    Bounds.sizeZ += 5;
    Bounds.sizeW += 5;
#else

    char start[8][8] = {
        {1, 0, 0, 0, 0, 0, 0, 1},
        {1, 1, 0, 1, 0, 0, 1, 0},
        {1, 0, 1, 0, 1, 1, 1, 0},
        {0, 1, 1, 0, 0, 0, 0, 0},
        {0, 1, 1, 0, 1, 0, 0, 0},
        {1, 1, 0, 1, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 0, 1, 0},
        {1, 1, 0, 1, 0, 1, 1, 1},
    };

    for(u32 i = 0; i < 8; ++i) {
        for(u32 j = 0; j < 8; ++j) {
            grid[get_coord((GRID_DIM/2)+i, (GRID_DIM/2)+j, GRID_DIM/2, GRID_DIM/2)] = start[j][i];
        }
    }
    // update bounds:
    // bounds need to inclue 1 more in each direction so we evaluate all square that can possibly change
    Bounds.minX = GRID_DIM/2 - 1;
    Bounds.minY = GRID_DIM/2 - 1;
    Bounds.minZ = GRID_DIM/2 - 1;
    Bounds.minW = GRID_DIM/2 - 1;
    Bounds.sizeY += 10;
    Bounds.sizeX += 10;
    Bounds.sizeZ += 3;
    Bounds.sizeW += 3;
#endif


    for (u32 cycle = 0; cycle < 6; ++cycle) { 
        printf("======Cycle %u ======\n\n", cycle);
        // 1. get neighbours
        char *neighbours = (char*)malloc(sizeof(char) * (Bounds.sizeW) * (Bounds.sizeZ) * (Bounds.sizeY) * (Bounds.sizeX));

        for(u32 w = 0; w < Bounds.sizeW; ++w) {
            for(u32 z = 0; z < Bounds.sizeZ; ++z) {
                for(u32 y = 0; y < Bounds.sizeY; ++y) {
                    for(u32 x = 0; x < Bounds.sizeX; ++x) {
                        u32 i = (w*Bounds.sizeZ*Bounds.sizeY*Bounds.sizeX) + (z*Bounds.sizeY*Bounds.sizeX) + (y * Bounds.sizeX) + x;
                        neighbours[i] = count_neighbours(grid, x+Bounds.minX, y+Bounds.minY, z+Bounds.minZ, w+Bounds.minW);
                        // printf("%u", (u32)neighbours[i]);
                    }
                    // printf("\n");
                }
                // printf("-----------------\n");
            }
        }


        // printf("=================\n");

        // 2. update grid
        for(u32 w = 0; w < Bounds.sizeW; ++w) {
            for(u32 z = 0; z < Bounds.sizeZ; ++z) {
                printf("z=%i, w=%i\n", z+Bounds.minZ-64, w+Bounds.minW-64);
                for(u32 y = 0; y < Bounds.sizeY; ++y) {
                    for(u32 x = 0; x < Bounds.sizeX; ++x) {
                        u32 i = (w*Bounds.sizeZ*Bounds.sizeY*Bounds.sizeX) + (z*Bounds.sizeY*Bounds.sizeX) + (y * Bounds.sizeX) + x;
                        char *gv = grid + get_coord(x+Bounds.minX, y+Bounds.minY, z+Bounds.minZ, w+Bounds.minW);
                        char nv = neighbours[i];
                        if(*gv && !(nv == 2 || nv == 3)) {
                            *gv = 0;
                        }
                        else if (*gv == 0 && nv == 3) {
                            *gv = 1;
                        }
                        printf("%c", *gv ? '#' : '.');
                    }
                    printf("\n");
                }
                printf("----------\n");
            }
        }
        free(neighbours);

        // grow the bounds?
        if(Bounds.minX == 0 || Bounds.minY == 0 || Bounds.minZ == 0 || Bounds.minW == 0) {
            printf("reached lower bound\n");
            exit(1);
        }
        if(Bounds.sizeX >= GRID_DIM || Bounds.sizeY >= GRID_DIM || Bounds.sizeZ >= GRID_DIM || Bounds.sizeW >= GRID_DIM) {
            printf("reached upper bound\n");
            exit(1);
        }

        Bounds.minX--;
        Bounds.minY--;
        Bounds.minZ--;
        Bounds.minW--;
        Bounds.sizeX += 2;
        Bounds.sizeY += 2;
        Bounds.sizeZ += 2;
        Bounds.sizeW += 2;
    }

    // count active cubes
    u32 count = 0;
    for(u32 w = 0; w < Bounds.sizeW; ++w) {
        for(u32 z = 0; z < Bounds.sizeZ; ++z) {
            for(u32 y = 0; y < Bounds.sizeY; ++y) {
                for(u32 x = 0; x < Bounds.sizeX; ++x) {
                    char *gv = grid + get_coord(x+Bounds.minX, y+Bounds.minY, z+Bounds.minZ, w+Bounds.minW);
                    count += (u32)(*gv);
                }
            }
        }
    }
    printf("final count is %u\n", count);

    // for(u32 z = 0; z < Bounds.sizeZ; ++z) {
    //     for(u32 y = 0; y < Bounds.sizeY; ++y) {
    //         for(u32 x = 0; x < Bounds.sizeX; ++x) {
    //             char *gv = grid + get_coord(x+Bounds.minX, y+Bounds.minY, z+Bounds.minZ);
    //         // grid[get_coord((GRID_DIM/2)+i, (GRID_DIM/2)+j, GRID_DIM/2)] = start[j][i];
    //         // printf("%u", grid[get_coord(GRID_DIM/2+j, GRID_DIM/2+i, GRID_DIM/2)]);
    //         u32 v = count_neighbours(grid, GRID_DIM/2+j, GRID_DIM/2+i, GRID_DIM/2);
    //         printf("%u,", v);
    //     }
    //     printf("\n");
    // }

    return 0;
}
