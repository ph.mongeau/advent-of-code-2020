.PHONY = run all-run

run: build/day18
	$<

DAYSCPP = $(wildcard day*.cpp)
DAYSEXEC = $(DAYSCPP:%.cpp=build/%)

all:  $(DAYSEXEC)
all-run: $(DAYSEXEC)
	@ $(foreach var,$(DAYSEXEC), echo "----$(var)-----" & ./$(var);)

clean:
	rm build/day*

# FLAGS=-g -fsanitize=address -std=c++11
# FLAGS= -g  -std=c++11
FLAGS= -g -O0

build/%: %.cpp
	@g++ $(FLAGS) $< -o $@
