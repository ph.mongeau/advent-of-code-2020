#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define PREAMBLE 25
#define DEG_TO_RAD 0.01745329

#define BUCKET_COUNT 300
#define BUCKET_SIZE BUCKET_COUNT*100

struct memory_entry {
    u64 key[BUCKET_SIZE];
    u64 value[BUCKET_SIZE];
    u64 nextFree;
};

struct memory_space {
    memory_entry *entries;
    u64 capacity;
};

void set_number(memory_space *memory, u64 key, u64 value) {
    u64 h = key % memory->capacity;
    memory_entry *e = &memory->entries[h];

    for(u64 i = 0; i < e->nextFree; ++i) {
        if (e->key[i] == key) {
            e->value[i] = value;
            return;
        }
    }

    if (e->nextFree >= BUCKET_SIZE) {
        printf("too many entries\n");
        exit(1);
    }

    else {
        e->key[e->nextFree] = key;
        e->value[e->nextFree] = value;
        e->nextFree++;
    }
}

u64 find_number(memory_space *memory, u64 key) {
    u64 h = key % memory->capacity;
    memory_entry *e = &memory->entries[h];

    if (!e) return 0ul;

    for(u64 i = 0; i < e->nextFree; ++i) {
        if (e->key[i] == key) {
            return e->value[i];
        }
    }
    return -1ul;
}

u64 get_next_number(u64 i, u64 prev, memory_space *memory) {
    // u64 prev = numbers[i-1];
    u64 idx = find_number(memory, prev);
    if(idx == -1ul) return 0;
    return i - idx - 1;

    // for(u32 j = i - 2; j >= 0;) {
    //     if (prev == numbers[j]) {
    //         return i - j - 1;
    //     }
    //     if (j == 0) break;
    //     j--;
    // }
    return 0;
}


int main() {
// #define TURNS 30_000_000
#define TURNS 30000000
// #define TURNS 2020
    // u64 *numbers = (u64*)malloc(sizeof(u64) * TURNS);
    u32 startCount = 6;

    u64 start[] = {9u,3u,1u,0u,8u,4u};
    // memcpy(numbers, start, sizeof(start) * ARRAY_COUNT(start));

    memory_space memory = {0};
    memory.capacity = BUCKET_COUNT;
    memory.entries = (memory_entry*)malloc(sizeof(memory_entry) * memory.capacity);
    memset(memory.entries, 0, sizeof(memory_entry) * memory.capacity);


    for(u32 i = 0; i < ARRAY_COUNT(start); ++i) {
        // set_number(&memory, start[i], i);
        // numbers[i] = start[i];
        u64 n  = find_number(&memory, start[i]);
        // printf("%u: %lu\n", i+1, numbers[i]);
        if (i > 0) set_number(&memory, start[i-1], i-1);
    }

    u64 n = 0;
    u64 prev = start[5];
    for(u64 i = 6; i < TURNS; ++i) {
        n = get_next_number(i, prev, &memory);
        set_number(&memory, prev, i-1);
        prev = n;
        // printf("%u: %u\n", i+1, numbers[i]);
    }
    printf("%u: %lu\n", TURNS, n);
}
