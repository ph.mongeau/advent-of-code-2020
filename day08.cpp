#include "utils.h"
#include <string.h>

enum OpCode {
    OpCode_NOP,
    OpCode_JMP,
    OpCode_ACC,
};


struct operation {
    OpCode op;
    s32 val;
    bool ran;
};

struct machine {
    operation *ops;
    u32 opcount;
    s32 accumulator;
};

u32 run(machine *prog, u32 instruction_index, s32 dir) {

    // reset the ops
    for(u32 i = 0; i < prog->opcount; ++i)
        prog->ops[i].ran = 0;
    
    for(;instruction_index < prog->opcount;) {
        operation *op = &prog->ops[instruction_index];

        if (op->ran) {
            break;
        }
        op->ran = 1;

        switch(op->op) {
            case OpCode_ACC:
                prog->accumulator += op->val;
                break;
            case OpCode_JMP:
                instruction_index += op->val - 1; // substract the +1 the gets added below
                break;
            default: break;
        }

        instruction_index += 1;
    }

    return instruction_index;
}


int main() {

    char *data;
    u32 fsize = read_file("day08_input.txt", &data);
    u32 lineCount = line_count(data, fsize);

    operation *program = (operation*)malloc(sizeof(operation) * lineCount);
    // clear program
    memset(program, 0, sizeof(operation) * lineCount);

    u32 opCount = 0;

    // parse the program
    char *ptr = data;
    while(*ptr) {
        char opstr[16];
        // s32 val;
        s32 offset;

        operation *op = &program[opCount++];
        // op->val = val;

        sscanf(ptr, "%s %i\n%n", opstr, &op->val, &offset);

        if (!strcmp("nop", opstr)) {
            op->op = OpCode_NOP;
        }
        else if (!strcmp("acc", opstr)) {
            op->op = OpCode_ACC;
        }
        else if (!strcmp("jmp", opstr)) {
            op->op = OpCode_JMP;
        }

        ptr += offset;
    }

    // u32 stack[lineCount];
    // u32 stackPtr = 0;

    machine m = {program, opCount};

    // part 1: run untill we hit an infinite loop
    run(&m, 0, 1);
    printf("part1: acc is %u\n", m.accumulator);

    // part 2: find a NOP or JMP we can swap to make the program run to the end
    u32 maxRanTo = 0;
    for (u32 i = 0; i < opCount; ++i) {

        m.accumulator = 0;

        operation *op = &m.ops[i];
        if (op->op == OpCode_JMP || op->op == OpCode_NOP) {

            // hack: NOP and JMP are 0 and 1 so we can toggle them
           op->op = (OpCode)(!op->op);

           u32 ranTo = run(&m, 0, 1);
           maxRanTo = ranTo > maxRanTo ? ranTo : maxRanTo;
           if (ranTo >= opCount) {
               printf("part 2: acc is %i\n", m.accumulator);
               break;
           }
           op->op = (OpCode)(!op->op);
        }
    }
    return 0;
}
