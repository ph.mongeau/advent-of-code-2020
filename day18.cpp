#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define GRID_DIM 128
#define GRID_SIZE GRID_DIM*GRID_DIM*GRID_DIM*GRID_DIM

struct state {
    char op;
    u64 lval, rval;
};

void eval_exp(state *s) {

    u64 result = s->lval;
    if(s->op == '*') {
        result *= s->rval;
    }
    else if (s->op == '+') {
        result += s->rval;
    }

    // printf("%lu %c %lu = %lu\n", s->lval, s->op, s->rval, result);

    s->lval = result;

    // clear the op
    s->op = 0;
    s->rval = -1;
}

char* parse_exp(char *c, state *s) {
    while(*c != '\n') {
        // skip any whitespace
        while(*c == ' ') c++;

        // parse a paren
        if(*c == '(') {
            state sub_state = {0};
            sub_state.rval = -1;
            c = parse_exp(++c, &sub_state);
            if(s->op == 0)
                s->lval = sub_state.lval;
            else
                s->rval = sub_state.lval;
        }
        else if(*c == ')') {
            return c;
        }
        // parse an op
        else if(*c == '+' || *c == '*') {
            s->op = *c;
        }
        // parse a number
        else if(*c >= '0' && *c <= '9') {
            char *end = c + find_char(c, ' ');

            if(s->op == 0)
                s->lval = strtoull(c, &end, 10);
            else  {
                s->rval = strtoull(c, &end, 10);
            }
        }

        // if we have an rval, we check the next token to see if it has hiher precedence
        if(s->rval != UINT64_MAX) {
            c++;
            while(*c == ' ') c++;
            if (s->op == '+'  || *c == 0 || *c == '*' || *c == '\n') {
                // next op has lower precedence so we can eval
                eval_exp(s);
            }
            else {
                // eval right side
                state sub_state = {0};
                sub_state.lval = s->rval;
                sub_state.rval = -1;
                c = parse_exp(c, &sub_state);
                s->rval = sub_state.lval;
                eval_exp(s);
            }
            continue;
        }


        // // if all the states are set we can eval the expression and reset the state
        // if(s->op && s->rval != UINT64_MAX) {
        //     eval_exp(s);
        // }

        // go to next character
        if (*c) c++;
        else break;
    };
    return c;
}


int main() {

    char *data;
    read_file("day18_input.txt", &data);

    u64 total = 0ul;
    for(char *ptr = data; *ptr; ptr++)
    {
        state s = {0};
        s.rval = -1;
        ptr = parse_exp(ptr, &s);
        printf("result %lu\n", s.lval);
        total += s.lval;
    }
    printf("total: %lu\n", total);

    return 0;
}
