#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define PREAMBLE 25
#define DEG_TO_RAD 0.01745329

int cmp_int(const void *a, const void *b) {
    return *((int*)a) > *((int*)b) ? 1 : -1;
}

void printGrid(char *grid, u32 lineCount, u32 columnCount) {
    for(u32 y = 1; y <= lineCount; ++y)
    {
        for(u32 x = 1; x <= columnCount; ++x)
        {
            char c = grid[y*(columnCount+2)+x];
            printf("%c", c);
        }
        printf("\n");
    }
}

bool findInDir(char *grid, u32 lineCount, u32 columnCount, u32 x, u32 y, u32 dx, u32 dy) {
    for(;;)  
    {
        x += dx;
        y += dy;
        char c = grid[y*(columnCount+2)+x];
        if (c == '#') return true;
        if (c == 0 || c == 'L') return false;

    }
    return false;
}
u32 getCount(char *grid, u32 lineCount, u32 columnCount, u32 x, u32 y) {
    u32 count = 
        + findInDir(grid, lineCount, columnCount, x, y, +0, +1)
        + findInDir(grid, lineCount, columnCount, x, y, +0, -1)
        + findInDir(grid, lineCount, columnCount, x, y, +1, +0)
        + findInDir(grid, lineCount, columnCount, x, y, -1, +0)
        + findInDir(grid, lineCount, columnCount, x, y, +1, +1)
        + findInDir(grid, lineCount, columnCount, x, y, -1, -1)
        + findInDir(grid, lineCount, columnCount, x, y, +1, -1)
        + findInDir(grid, lineCount, columnCount, x, y, -1, +1);
    return count;
}

int main() {

    char *data;
    u32 fsize = read_file("day12_input.txt", &data);
    u32 lineCount = line_count(data, fsize);

    s32 x = 0, y = 0;
    s32 vx = 10, vy = 1;
    s32 angle = 90;
    // s32 wx = 10;
    // s32 wy = 1;

    printf("we are at %i, %i\n", x, y);
    for(char *ptr = data; *ptr && (ptr-data) < fsize;) {
        char c;
        s32 v, offset;
        sscanf(ptr, "%c%i\n%n", &c, &v, &offset);
        // printf("%c %u\n", c, v);
        ptr += offset;

        printf("%c%i\n", c,v);

        switch(c) {
            case 'N': vy += v; break;
            case 'S': vy -= v; break;
            case 'E': vx += v; break;
            case 'W': vx -= v; break;
            case 'F': x += v * vx; y += v * vy; break;
            case 'R':
            case 'L':
                // angle += v * (c == 'R' ? 1 : -1);
                v = v * (c == 'R' ? -1 : 1);


                // printf("waypoint was at %i, %i\n", vx, vy);
                // build a rotation matrix?
                s32 msin = sinf((f32)v * DEG_TO_RAD);
                s32 mcos = cosf((f32)v * DEG_TO_RAD);
                s32 px = vx;
                s32 py = vy;
                vx = px * mcos - py * msin;
                vy = px * msin + py * mcos;

                // vx = sinf((f32)angle * DEG_TO_RAD) * vx;
                // vy = cosf((f32)angle * DEG_TO_RAD) * vy;
                // printf("angle: %i, vx: %i, vy: %i ", angle, vx, vy);
                // printf("waypoint is at %i, %i\n", vx, vy);
                break;
        }
        printf("we are at %i, %i\n", x, y);
        printf("waypoint is at %i, %i\n", vx, vy);
    }
    printf("the sum is %u\n", abs(x) + abs(y));

    // printf("number of occupied seats: %u\n", occupiedCount);
}
