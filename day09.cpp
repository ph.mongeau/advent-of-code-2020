#include "utils.h"
#include <string.h>

#define PREAMBLE 25

int main() {

    char *data;
    u32 fsize = read_file("day09_input.txt", &data);
    u32 lineCount = line_count(data, fsize);
    u32 numbers[lineCount];
    { // load the numbers
        u32 i = 0;
        u32 offset = 0;
        for(char *ptr = data; *ptr;) {
            sscanf(ptr, "%u\n%n", &numbers[i++], &offset);
            ptr += offset;
        }
    }

    u32 targetNumber = 0;

    for(u32 i = PREAMBLE; i < lineCount; ++i) {

        u32 found = 0;
        for(u32 j = i-PREAMBLE; j < i; ++j) {
            for(u32 k = i-PREAMBLE; k < i; ++k) {
                if(numbers[j] + numbers[k] == numbers[i]) {
                    found = 1;
                    break;
                }
            }
        }
        if (!found) {
            targetNumber = numbers[i];
            printf("part one: %u\n", targetNumber);
        }

    }

    // find n contiguous numbers that add up to the targetNumber
    for(u32 i = 0; i < lineCount-1; ++i) {
        // we can ignore any number larger the tne targetNumber
        if (numbers[i] > targetNumber) continue;

        u32 sum = 0;

        u32 min = targetNumber;
        u32 max = 0;

        for(u32 j = i+1; j < lineCount; ++j) {
            // keep track of min and max numbers in the range
            min = numbers[j] < min ? numbers[j] : min;
            max = numbers[j] > max ? numbers[j] : max;

            sum += numbers[j];
            if (sum == targetNumber) {
                printf("we got it [%u, %u], %u\n", i,j, max+min);
            }
            else if (sum > targetNumber) {
                break;
            }
        }
    }

    return 0;
}
