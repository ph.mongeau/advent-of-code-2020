#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int32_t s32;
typedef int64_t s64;
typedef float f32;
typedef double f64;

#define MIN(a,b) a < b ? a : b
#define MAX(a,b) a >= b ? a : b

#define ARRAY_COUNT(a) (sizeof(a)/sizeof(a[0]))

inline
u32 read_file(const char *file_path, char **dest)
{
    FILE *fp = fopen(file_path, "r");
    fseek(fp, 0, SEEK_END);
    int fsize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    *dest = (char*)malloc(sizeof(char)*(fsize+1));
    fread(*dest, sizeof(char), fsize, fp);
    fclose(fp);
    return fsize;
}

inline
u32 line_count(char *data, int size)
{
    int count = 0;
    for(char *ptr = data; (ptr-data) < size && *ptr; ++ptr)
    {
        if (*ptr == '\n') ++count;
    }
    return count;
}

inline
s32 find_char(char *data, char c)
{
    uint32_t index = 0;
    for(char *ptr = data; *ptr; ++ptr)
    {
        if (*ptr == c) break;
        index++;
    }
    /* if (*ptr != c) index = -1; */
    return index;
}


/* void read_file_lines(const char *file_path, char **dest) { */
/* { */
/*     char *data; */
/*     read_file(file_path, &data); */
/*     return; */
/* } */
