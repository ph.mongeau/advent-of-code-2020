#include "utils.h"
#include <string.h>


// a bag has a name and can contain at most 1024 other types of bags
struct bag {
    char name[1024];
    char canContain[1024][128];
    u32 canContainCount;
    u32 requiredAmounts[1024];
};


void parse_line(char *line, bag *Bag) {

    char *eol = line + find_char(line, '\n');

    char *n, *ptr;
    ptr = line;

    // parse the name
    n = strstr(ptr, " bag");
    memcpy(Bag->name, ptr, sizeof(char) * (n - ptr));

    ptr = strstr(n, "contain ") + 8; // move past "contain "

    // start parsing contents
    char *no = strstr(ptr, "no other");
    if(no && no < eol) return;

    while(ptr < eol)
    {
        u32 count;
        u32 parsedLen;
        sscanf(ptr, "%u %n", &count, &parsedLen);
        ptr += parsedLen;

        char *b = Bag->canContain[Bag->canContainCount++];
        Bag->requiredAmounts[Bag->canContainCount-1] = count;

        // parse bag color
        n = strstr(ptr, " bag");
        memcpy(b, ptr, sizeof(char) * (n - ptr));

        ptr = n +  6; // move past " bag."
    }
}

bag* findBag(bag *allBags, u32 allBagCount, char *name) {
    bag *Result = 0;
    for (int i = 0; i < allBagCount; ++i) {
        if (!strcmp(allBags[i].name, name)) {
            Result = &allBags[i];
            break;
        }
    }
    return Result;
}

int canContain(bag Container, char *name, bag *allBags, u32 allBagCount) {
    for (int j = 0; j < Container.canContainCount; ++j)
    {
        char *contentName = Container.canContain[j];
        if(strcmp(contentName, name) == 0) {
            return 1;
        }
        bag *b = findBag(allBags, allBagCount, contentName);
        if(b && canContain(*b, name, allBags, allBagCount))
            return 1;
    }
    return false;
}

u32 contentCount(bag Container, bag *allBags, u32 allBagCount) {
    u32 sum = 1;
    // u32 sum = Container.requiredTotal;

    for (int j = 0; j < Container.canContainCount; ++j)
    {
        bag *b = findBag(allBags, allBagCount, Container.canContain[j]);
        if(b) sum += Container.requiredAmounts[j] * contentCount(*b, allBags, allBagCount);
    }
    return sum;
}

int main() {

    char *data;
    u32 fsize = read_file("day07_input.txt", &data);
    // printf("%s\n", data);


    // parse a key
    char *ptr = data;

    bag *bags = (bag*)malloc(sizeof(bag) * 1024);
    int bagCount = 0;

    while(*ptr) {
        char bufferA[BUFSIZ] = {};
        char bufferB[BUFSIZ] = {};
        u32 parsedSize;

        char *next = ptr + find_char(ptr, '\n') + 1;
        parse_line(ptr, &bags[bagCount++]);
        ptr = next;
    }

    char rootName[] = "shiny gold";

    u32 validCount = 0;
    for (u32 i = 0; i < bagCount; ++i)
    {
        bag Bag = bags[i];
        if (canContain(Bag, rootName, bags, bagCount)) {
            // printf("bag %s \n", Bag.name);
            validCount++;
        }
    }
    printf("\nvalidCount: %u\n", validCount);

    bag *rootBag = findBag(bags, bagCount, rootName);
    printf("number of bags in shiny %u\n", contentCount(*rootBag, bags, bagCount) - 1);

    return 0;
}
