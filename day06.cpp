#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "utils.h"

int main() {

    char *data;
    u32 fsize = read_file("day06_input.txt", &data);

    u8 q[26] = {0};
    u32 group_size = 1;

    u32 sum1 = 0;
    u32 sum2 = 0;

    for(char *ptr = data; (ptr - data) <= fsize; ++ptr)
    {
        if(group_size && *ptr == '\n' && (*(ptr+1) == '\n' || !*(ptr+1)))
        {
            // do the count
            for (u32 i= 0; i< 26; ++i){
                sum2 += q[i] == group_size;
                sum1 += q[i] > 0;
            }
            // clear the questions
            for (u32 i= 0; i< 26; ++i) q[i] = 0;
            // reset group fsize
            group_size = 1;
            // skip the two line feeds
            ptr += 2;
        }

        if (*ptr == '\n') {
            group_size++;
            ptr++;
        }
        q[*ptr - 'a'] += 1;
    }
    printf("sum1 %i\n", sum1);
    printf("sum2 %i\n", sum2);
    return 0;
}
