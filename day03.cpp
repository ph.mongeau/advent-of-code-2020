#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "utils.h"

int main() {

    char *data;
    int fsize = read_file("day03_input.txt", &data);

    int rows = line_count(data, fsize);

    int32_t lineLength =  find_char(data, '\n') + 1;

    int slopes[][2] = {
        {1,1},
        {1,3},
        {1,5},
        {1,7},
        {2,1},
    };

    uint32_t product = 1;
    for (int s = 0; s < ARRAY_COUNT(slopes); ++s)
    {
        uint32_t treeCount = 0;
        int *slope = (int*)&(slopes[s]);
        for(int i = 0; i < rows/slope[0]; ++i) {
            char* line = (data + i*slope[0]*lineLength);

            int col = (i*slope[1]) % (lineLength-1);
            if (line[col] == '#') treeCount++;

            // debug print grid
            // line[col] = 'X';
            // printf("%.31s\n", line);
        }
        printf("treeCount %i\n", treeCount);
        product = product * treeCount;
    }
    printf("prod %u \n", product);

    free(data);
    return 0;
}
