import itertools


def set_permutations(addr, permutables, startIndex, value, memory):
    if startIndex > len(permutables):
        return

    memory[addr] = value

    # for i in range(startIndex, len(permutables)):
    for i in range(startIndex, len(permutables)):
        idx = permutables[i]

        tmp = addr & ~(1 << idx)
        if(tmp != addr):
            memory[tmp] = value

        set_permutations(tmp, permutables, i+1, value, memory)

        tmp = addr | (1 << idx)
        if(tmp != addr):
            memory[tmp] = value

        set_permutations(tmp, permutables, i+1, value, memory)

with open("day14_input.txt") as f:
    lines = list(f)

memory = {}

for l in lines:
    token = l.split(' ')
    if token[0] == 'mask':
        # set the mask
        mask = token[-1].strip()
    else:
        value = int(token[-1].strip())
        addr = int(token[0].split('[')[-1].split(']')[0])

        # part 1
        # memory[addr] = value

        # for i,c in enumerate(mask[::-1]):
        #     if c == 'X': continue

        #     if c == '1':
        #         memory[addr] |= (1<<i)
        #     else:
        #         memory[addr] &= ~(1<<i)

        for i,c in enumerate(mask[::-1]):
            if c == '1':
                addr |= (1 << i)

        permutables = [i for i in range(36) if mask[35-i] == 'X']
        set_permutations(addr, permutables, 0, value, memory)

        # memory[addr] = value
        # print('setting', addr, bin(addr))
        # for permuttion in itertools.product([0,1], repeat=len(permutables)):
        #     a = addr
        #     for idx,b in zip(permutables, permuttion):
        #         if b:
        #             a |= (1 << idx)
        #         else:
        #             a &=  ~(1 << idx)
        #     # print('setting', a, bin(a))
        #     memory[a] = value
        # print ('----')

solution = 4355897790573
print(sum(memory.values()), '==', solution)
