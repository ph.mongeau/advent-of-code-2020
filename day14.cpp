#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define PREAMBLE 25
#define DEG_TO_RAD 0.01745329

#define BUCKET_SIZE 1024
#define BUCKET_COUNT 4096

struct memory_entry {
    u64 addr[BUCKET_SIZE];
    u64 value[BUCKET_SIZE];
    u64 nextFree;
};

struct memory_space {
    memory_entry *entries;
    u64 capacity;
    u64 minAddr;
    u64 maxAddr;
    u64 *addresses;
    u64 nextAddr;
};

void set_memory(memory_space *memory, u64 addr, u64 value) {
    memory->minAddr = MIN(memory->minAddr, addr);
    memory->maxAddr = MAX(memory->maxAddr, addr);



    u64 h = addr % memory->capacity;
    memory_entry *e = &memory->entries[h];

    for(u64 i = 0; i < e->nextFree; ++i) {
        if (e->addr[i] == addr) {
            e->value[i] = value;
            return;
        }
    }

    if (e->nextFree >= 1024) {
        printf("too many entries\n");
        exit(1);
    }

    else {
        memory->addresses[memory->nextAddr++] = addr;
        e->addr[e->nextFree] = addr;
        e->value[e->nextFree] = value;
        e->nextFree++;
    }
}
u64 get_memory(memory_space *memory, u64 addr) {
    u64 h = addr % memory->capacity;
    memory_entry *e = &memory->entries[h];

    if (!e) return 0ul;

    for(u64 i = 0; i < e->nextFree; ++i) {
        if (e->addr[i] == addr) {
            return e->value[i];
        }
    }
    return 0ul;
}

void printbin(u64 v) {
    u64 mask = 1 << (sizeof(int) * CHAR_BIT - 1);
    while(mask) {
        printf("%d", ((v & mask) ? 1 : 0));
        mask >>= 1;
    }
    printf("\n");
}

void set_permutations(u64 addr, u64 *permutables, u64 startIndex, u64 permutationCount, u64 value, memory_space *memory) {
    if (startIndex >= permutationCount) return;

    // memory[addr] = value;
    set_memory(memory, addr, value);

    for(u64 i = startIndex; i < permutationCount; ++i) {
        u64 idx = permutables[i];
        u64 tmp = addr & ~(1UL << idx);
        if (tmp != addr) {
            // printbin(tmp);
            // memory[tmp] = value;
            set_memory(memory, tmp, value);
        }
        set_permutations(tmp, permutables, i+1, permutationCount, value, memory);

        tmp = addr | (1UL << idx);
        if (tmp != addr) {
            // printbin(tmp);
            // memory[tmp] = value;
            set_memory(memory, tmp, value);
        }
        set_permutations(tmp, permutables, i+1, permutationCount, value, memory);
    }
}

int main() {

    char *data;
    u64 fsize = read_file("day14_input.txt", &data);
    u64 lineCount = line_count(data, fsize);

    memory_space memory = {0};
    memory.minAddr = -1;
    memory.maxAddr = 0;

    memory.capacity = BUCKET_COUNT;
    memory.entries = (memory_entry*)malloc(sizeof(memory_entry) * memory.capacity);
    memset(memory.entries, 0, sizeof(memory_entry) * memory.capacity);
    memory.addresses = (u64*)malloc(sizeof(u64) * BUCKET_COUNT*BUCKET_SIZE);
    // memory.addresses =  (u32*)malloc(sizeof(memory.addresses) * 99999);
    // memory.values =  (u64*)malloc(sizeof(memory.addresses) * 99999);

    // memset(memory.values, 0UL, sizeof(u64)*99999);
    // u64 memory[99999] = {0ul};
    // u64 memory[99999] = {0ul};

    // {
    // u64 total = 0;
    // for(u32 i = 0; i < ARRAY_COUNT(memory); ++i) {
    //     total += memory[i];
    // }
    // printf("total is %lu\n", total);
    // }

    char mask[] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X";
    for(char *ptr=data; (ptr-data) < fsize && *ptr;)
    {
        // fprintf(stderr, "\r%lu / %lu", ptr-data, fsize);
        char op[128];
        char v[128];
        u32 offset;
        sscanf(ptr, "%s = %s\n%n", op, v, &offset);
        if(strcmp(op, "mask") == 0) {
            // update mask
            memcpy(mask, v, sizeof(char) * 36);
        }

        else {
            u64 addr = 0;
            if(!sscanf(op, "mem[%lu]", &addr)) {
                printf("failed to parse addr\n");

            }

            u64 value = 0;
            if(!sscanf(v, "%lu", &value)) {
                printf("failed to parse value\n");
            }

            // if(addr > 99999) {
            //     printf("make the array bigger you dumdum!\n");
            // }

            // PART1 apply the mask
            /*
            for(u64 i = 0; i < 36; ++i) {
                char c = mask[35-i];
                // printf("%c,", c);
                if (c == 'X') continue;
                else if (c == '1') //set 
                    memory[addr] |= (1UL << i);
                else if(c == '0')// clear
                    memory[addr] &= ~(1UL << i);
                else printf("invalid mask char %c\n", c);
            } */

            // u32 addresses[1024] = {0};
            // addresses[0] = addr;
            // // for(u32 i = 0; i < 1024; ++i) {
            // //     addresses[i] = addr;
            // // }
            u64 permutables[36] = {0};
            u64 permutationCount = 0;

            // PART2 apply the mask to the address
            // and get permutables bits;
            for(u64 i = 0; i < 36; ++i) {
                char c = mask[35-i];
                // if(c == '0') continue;
                if (c == '1') {
                    addr |= (1UL << i);
                }
                else if (c == 'X') {
                    permutables[permutationCount++] = i;
                }
            }
            set_permutations(addr, permutables, 0, permutationCount, value, &memory);


        }
        ptr += offset;
    }
    printf("done computing\n");
    u64 total = 0;
    // for(u64 i = 0; i < memory.capacity; ++i) {
    //     memory_entry e = memory.entries[i];
    //     for(u64 j = 0; j < e.nextFree; ++j) total += e.value[j];
    // }
    for(u64 i = 0; i < memory.nextAddr; ++i) {
        // memory_entry e = memory.entries[i];
        // for(u64 j = 0; j < e.nextFree; ++j) total += e.value[j];
        // fprintf(stderr, "\rcounting %lu / %lu", i, memory.nextAddr);
        total += get_memory(&memory, memory.addresses[i]);
    }

    printf("total is %lu\n", total);
}
