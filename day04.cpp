#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "utils.h"

struct passport {
    int byr;
    int iyr;
    int eyr;
    int hgt;
    int hcl;
    int ecl;
    int pid;
    int cid;
};

int isValidPassport(passport p) {
    return (
         p.byr &&
         p.iyr &&
         p.eyr &&
         p.hgt &&
         p.hcl &&
         p.ecl &&
         p.pid
    );

}

int main() {

    char *data;
    int fsize = read_file("day04_input.txt", &data);
    // printf("%s\n", data);
    uint32_t offset = 0;

    passport currentPassport = {};
    uint32_t validCount = 0;

    // parse a key
    while(offset < fsize) {

        char bufferA[4] = {};
        char bufferB[128] = {};

        int parsedLength = 0;
        int parsedCount = sscanf(data+offset, "%3s:%s%n", (char*)(&bufferA), (char*)(&bufferB), &parsedLength);
        if (parsedCount < 2)
        {
            validCount += isValidPassport(currentPassport);
            currentPassport = {};
            printf("\r%u %u", validCount, offset);
            break;
        }

        offset += parsedLength;
        // printf("%s %s\n", bufferA, bufferB);

        if(!strcmp("byr", bufferA)) {
            int byr = atoi(bufferB);
            if (byr >= 1920 && byr <= 2002) currentPassport.byr = 1;
        }
        if(!strcmp("iyr", bufferA)){
            int iyr = atoi(bufferB);
            if (iyr >=  2010 && iyr <= 2020) currentPassport.iyr = 1;
        }
        if(!strcmp("eyr", bufferA)) {
            int eyr = atoi(bufferB);
            if (eyr >=  2020 && eyr <= 2030) currentPassport.eyr = 1;
        }
        if(!strcmp("hgt", bufferA)) {
            int val = 0;
            char unit[2] = {};
            if (sscanf(bufferB, "%u%2s", &val, unit)) {
                if (!strcmp("in", unit) && val >= 59 && val <= 76)
                    currentPassport.hgt = 1;
                else if (!strcmp("cm", unit) && val >= 150 && val <= 193)
                    currentPassport.hgt = 1;
            }
        }
        if(!strcmp("hcl", bufferA)) {
            char col[6];
            if(sscanf(bufferB, "#%6s", col))
            {
                int isValid = 0;
                for(int i = 0; i < 6; ++i)
                {
                    isValid = ((col[i] >= '0' && col[i]  <= '9')||
                               (col[i] >= 'a' && col[i] <= 'f'));
                }
                currentPassport.hcl = isValid;
            }
            // else {
            //     printf("invalid hcl");
            // }
        }
        if(!strcmp("ecl", bufferA)) {
            int isValid = (!(strcmp("amb", bufferB)) ||
                           !(strcmp("blu", bufferB)) ||
                           !(strcmp("brn", bufferB)) ||
                           !(strcmp("gry", bufferB)) ||
                           !(strcmp("grn", bufferB)) ||
                           !(strcmp("hzl", bufferB)) ||
                           !(strcmp("oth", bufferB)));
            currentPassport.ecl = isValid;
        };
        if(!strcmp("pid", bufferA)){
            currentPassport.pid = strlen(bufferB) == 9;
        }
        if(!strcmp("cid", bufferA)) currentPassport.cid = atoi(bufferB);



        if (*(data+offset) == ' ') offset++;
        if (*(data+offset) == '\n' && *(data+offset+1) == '\n') {
            int v = isValidPassport(currentPassport);
            // if (!v){
            //     printf("invalid");
            // }
            validCount += v;
            currentPassport = {};
            printf("\r%u %u", validCount, offset);
        }
    }

    printf("\nvalidCount: %u\n", validCount);
    return 0;
}
