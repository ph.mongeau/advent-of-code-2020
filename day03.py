f = open('day3_input.txt')
lines = f.read().split('\n')
slopes = [
    (1,1),
    (1,3),
    (1,5),
    (1,7),
    (2,1),
]

counts = []
for slope in slopes:
    count = 0
    for i in range(0, len(lines)//slope[0]):
        l = lines[i*slope[0]]
        if not l:
            break
        l = list(l)

        if l[(i*slope[1])%len(l)] == '#':
            count += 1

        # l[(i*slope[1])%len(l)] = 'X'
        # lines[i*slope[0]] = ''.join(l)
    # print(slope, count)
    counts.append(count)

# print('\n'.join(lines))

total = 1
for c in counts:
    total *= c

print(total)
