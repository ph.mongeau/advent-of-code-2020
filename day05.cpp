#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "utils.h"

int binary_search(char *code, int codeLen, char c, int min, int max) {
    for(int i = 0; i < codeLen; ++i)
    {
        if(code[i] == c) {
            max = min + (max - min) / 2;
        }
        else {
            min = 1 + min + (max - min) / 2;
        }
        // printf("%c %i %i\n", code[i], min, max);
    }
    return min;
}
int seat_id(char *code) {
    int row = binary_search(code, 7, 'F', 0, 127);
    int col = binary_search(code+7, 3, 'L', 0, 7);
    return 8 * row + col;
}

int main() {

    char *data;
    int fsize = read_file("day05_input.txt", &data);

    int seats[1024] = {};

    int max_id = 0;
    for(char *ptr = data; (ptr - data) < fsize;)
    {
        char test[128] = {};
        int offset = 0;
        sscanf(ptr, "%s\n%n", test, &offset);
        int id = seat_id(test);
        if (id > max_id) max_id = id;

        seats[id] = 1;
        // printf("%s\n", test);
        // ptr += 11;
        ptr += offset;
    }
    printf("max_id: %i\n", max_id);
    for (int i = 0; i < 1024; ++i) {
        if (seats[i] == 0 && seats[i+1] && seats[i-1]) {
            printf("this seat is empty: %i\n", i);
        };
    }
    return 0;
}
