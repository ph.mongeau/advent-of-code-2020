#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

#define ARRAY_COUNT(a) (sizeof(a)/sizeof(a[0]))


int main() {
    char *data;
    int fsize = read_file("day01_input.txt", &data);
    int count = line_count(data, fsize);
    int *test_data = (int*)malloc(sizeof(int)*(count+1));
    int j = 0;

    for(char *ptr = data; (ptr-data) < fsize && *ptr;) {
        char buffer[128] = {};
        int i = 0;
        for(i = 0; *(ptr+i) && *(ptr+i) != '\n'; ++i) {
            buffer[i] = *(ptr+i);
        }
        if (*buffer) {
            test_data[j++] = atoi(buffer);
            ptr += i;
            if(*ptr == '\n') ptr++;
        }
        else {
            break;
        }
    }
    free(data);

    for (int i=0; i < count; ++i)
    {
        for (int j=1; j < count; ++j)
        {
            for (int k=2; k < count; ++k)
            {
            
                int a = test_data[i];
                int b = test_data[j];
                int c = test_data[k];

                if (i==j || i == k || j == k) continue;

                // printf("test\n");

                if(a+b+c == 2020) {
                    printf("%i + %i + %i == 2020\n", a,b,c);
                    printf("%i * %i * %i == %i\n", a,b,c, a*b*c);
                    free(test_data);
                    return 0;
                }
            }
        }
    }
    free(test_data);
    return 1;
}
